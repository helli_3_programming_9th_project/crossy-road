from ..Bases.gameObject import GameObject


class UIObject(GameObject):
    pass


class Button(UIObject):
    pass
