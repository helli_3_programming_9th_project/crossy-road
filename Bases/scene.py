from .tabel import Table
from .settings import DONE
from .gameObject import GameObject
from .tabel import Table


class Scene:
    def __init__(self):
        self._scene = Table(["label", "game object"])
        pass

    def add(self, new_game_object):
        row_dictionary = {
            "label": new_game_object.label,
            "game object": new_game_object
        }
        return self._scene.create(row_dictionary)

    def find_game_object_with_label(self, label):
        where_dictionary = {"label": label}
        return self._scene.read(where_dictionary)[0]

    def update(self):
        for g in self._scene.read_column("game object"):
            g.update()

    def draw(self, page):
        for g in self._scene.read_column("game object"):
            g.draw(page)
