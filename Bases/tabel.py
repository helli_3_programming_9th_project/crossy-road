from . import settings


class Table:
    def __init__(self, column_list):
        self._columns = {}
        self._count = 0
        for column in column_list:
            self._columns[column] = []
        pass

    def create(self, row_dictionary):
        validation_result, message = self.__validate_row_dictionary(
            row_dictionary)
        if not validation_result:
            raise Exception("Validation Error: " + message)
        for key in self._columns:
            self._columns[key].append(row_dictionary[key])
        self._count += 1
        return settings.DONE

    def read(self, where_dictionary):
        key_in_table, message = self.__is_keys_in_columns(where_dictionary)
        if not key_in_table:
            return False, message

        result = []
        ids = []
        temp_key = list(where_dictionary.keys())[0]
        temp_value = where_dictionary[temp_key]
        possibilities = self.__find_index_all(temp_key, temp_value)
        for possible in possibilities:
            row = {}
            for key in where_dictionary:
                value = where_dictionary[key]
                in_table_value = self._columns[key][possible]
                if value == in_table_value:
                    row[key] = value
            if len(row) == len(where_dictionary):
                row = self.select_by_id(possible)
                result.append(row)
                ids.append(possible)
        return result, ids

    def read_column(self, column_key):
        if not (column_key in self._columns):
            raise Exception()
        return self._columns[column_key]

    def update(self, where_dictionary, set_dictionary):
        key_in_table, message = self.__is_keys_in_columns(set_dictionary)
        if not key_in_table:
            return False, message

        ids = self.read(where_dictionary)[1]
        for id in ids:
            for e in set_dictionary:
                self._columns[e][id] = set_dictionary[e]
        return settings.DONE

    def delete(self, where_dictionary):
        ids = self.read(where_dictionary)[1]
        ids.reverse()
        for id in ids:
            for col in self._columns:
                del self._columns[col][id]
            self._count -= 1
        return settings.DONE

    def select_by_id(self, id):
        row = {}
        for column in self._columns:
            row[column] = self._columns[column][id]
        return row

    def get_column_label(self):
        return self._columns.keys()

    def __find_index_all(self, column_key, value):
        result = []
        counter = 0
        for e in self._columns[column_key]:
            if e == value:
                result.append(counter)
            counter += 1
        return result

    def __validate_row_dictionary(self, row_dictionary):
        if len(row_dictionary) != len(self._columns):
            return False, settings.LENGHT_ERROR
        key_in_table, message = self.__is_keys_in_columns(row_dictionary)
        if not key_in_table:
            return False, message
        return True, settings.DONE

    def __is_keys_in_columns(self, dictionary):
        for key in dictionary:
            if not key in self._columns:
                return False, "{key} not found in table!".format(key=key)
        return True, settings.DONE

    def __len__(self):
        return self._count

    def __str__(self):
        result = ""
        for column in self._columns:
            result += column + " | "
        result = result + "id\n" + (len(result) + 2) * "-"
        for i in range(len(self)):
            dic = self.select_by_id(i)
            result += "\n"
            for cell in dic:
                c = dic[cell]
                if len(c) <= len(cell):
                    result += c + " " * (len(cell) - len(c)) + " | "
                else:
                    result += dic[cell][:len(cell)] + " | "
            result += str(i)
        return result
