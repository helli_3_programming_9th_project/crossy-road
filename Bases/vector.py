class Vector:
    def __init__(self, *args):
        self._args = args
        pass

    def __len__(self):
        return len(self._args)

    def __add__(self, other):
        if len(self) != len(other):
            raise Exception()
        return tuple([i + j for i, j in zip(self._args, other)])


class Vector2(Vector):
    def __init__(self, *args):
        super().__init__(args[0], args[1])
        pass

    @property
    def x(self):
        return self._args[0]

    @property
    def y(self):
        return self._args[1]
