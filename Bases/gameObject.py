from .vector import Vector2
from . import settings
import pygame


class GameObject:
    def __init__(self, position=(0, 0), scale=(0, 0), color=(0, 0, 0), label=""):
        self._position = Vector2(position[0], position[1])
        self._scale = Vector2(scale[0], scale[1])
        self._color = color
        self.label = label
        pass

    def change_position(self, new_x, new_y):
        self._position = Vector2(new_x, new_y)
        return settings.DONE

    def change_scale(self, new_x, new_y):
        self._scale = Vector2(new_x, new_y)
        return settings.DONE

    def draw(self, page):
        ps = (self._position.x, self._position.y, self._scale.x, self._scale.y)
        pygame.draw.rect(page, self._color, ps)
        return settings.DONE

    def update(self):
        pass
