from Bases.scene import Scene
from Bases.gameObject import GameObject
from Bases.vector import Vector2
from Bases import settings
import pygame


class Player(GameObject):
    def __init__(self, position):
        super().__init__(position=position, scale=(40, 40),
                         color=(0, 250, 0), label="player")

    def update(self):
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    self._position = Vector2(
                        self._position.x, self._position.y - 50)
                elif event.key == pygame.K_DOWN:
                    self._position = Vector2(
                        self._position.x, self._position.y + 50)
        return settings.DONE


class Car(GameObject):
    def __init__(self, position, label="car", area=(0, 800), speed=1):
        super().__init__(position=position, scale=(40, 40),
                         color=(200, 0, 0), label=label)
        self._area = Vector2(area[0], area[1])
        self._speed = speed

    def update(self):
        if self._position.x > 800:
            self._position = Vector2(-40, self._position.y)
        self._position = Vector2(
            self._position.x + self._speed, self._position.y)
        return settings.DONE


page = pygame.display.set_mode((800, 500))

pygame.init()
location = ()
done = False

play_scene = Scene()
for i in range(8):
    c = Car((0, (i + 1) * 50), "car " + str(i))
    play_scene.add(c)
player = Player(position=(380, 450))
play_scene.add(player)


while not done:
    page.fill((0, 255, 255))
    play_scene.update()
    play_scene.draw(page)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
    pygame.display.flip()

pygame.quit()
